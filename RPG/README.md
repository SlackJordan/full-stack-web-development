# Javascript Text Based RPG
 
## Summary
I created this RPG with Javascript, html and css in order to complete the first step of this youtube tutorial on full-stack web development.

The section that covers this is located here with timestamp:

https://youtu.be/nu_pCVPKzTk?si=iPkGPCIzx2sJpTmZ&t=11749

## Lessons Learned
* Manipulation of css with js ex. showing boxes based on conditions
* JavaScript syntax for things such as:
  * for loops
  * while loops
  * array creation and assignment
  * creation functions and how to use lambda
  * using the math library to create random numbers.
* How to get objects with Javascript and Manipulate them
  * Example, showing how to grab a button for further manipulation. 
     ```
     const button1 = document.querySelector("#button1");
     ```
  * Example showing this assigned to change based off values in an array after click.
    ```
    button1.innerText = location["button text"][0];
    ```
## Take Aways

I'm happy to say that I enjoyed this lesson much more than other JavaScript courses I've taken in the past. I really think their approach at going straight into project based examples really helped me see how everything works with DOM manipulation. Thanks again FreeCodeCamp for providing such awesome training for free. 